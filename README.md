# 25Live Events Module

## Connects to the 25Live events API to read and display events locally, avoiding the horrendous spud interface

## Releases

* 0.9.0 - Proof of concept release. Connects to the API and lists some events in a Block.

## ToDos

* [] Better error control in the API Connection Class (R25LiveConnection)
* [] Output on the main events page.
* [] Handle an event id input on the events page to load that event.
* [] Provide interface for filtering event categories.
* [] Handle display of all events for a given organization.
* [] Calendar interface.

## Testing

### Setup

The module needs to be installed in a valid Drupal instance which has a valid phpunit installation.

All drupal phpunit tests are run from the `web/core` directory.
If a `phpunit.xml` configuration file does not live there the `-c <path to phpunit.xml>` option must be used in the command.

### Running the tests

From the `web/core` direcotry run:

```console
../../vendor/bin/phpunit -c ../../phpunit.xml ../modules/custom/twenty_five_live_events/
```
